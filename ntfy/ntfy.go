package ntfy

import (
	"net/http"
	"strings"
)

// SendNtfyNotification sends notification to ntfy
func SendNtfyNotification(message string, ntfyURL string, click string) error {
	req, err := http.NewRequest("POST", ntfyURL,
		strings.NewReader(message))
	if err != nil {
		return err
	}
	req.Header.Set("Title", "[mkm-notifier] Card found!")
	req.Header.Set("Click", click)

	_, err = http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	return nil
}
