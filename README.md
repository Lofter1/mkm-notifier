# MKM-Notifier

MKM-Notifier is a program that will look at a MKM-Seller and notify you when one of the specified cards is available.

## Installation

You can install MKM-Notifier through Go by executing

```shell
go install gitlab.com/Lofter1/mkm-notifier
```

You also need the ntfy app.

## Setup

In order for mkm-notifier to work as it should, you need to set up a way to execute it regularly.
A Cron-Job would be perfect for this, but there are other ways.

Next you need to set up a ntfy topic. This topic needs to be unique, so that nobody else gets your notifications.
You Could choose something like `mkm-<your name>-<random numbers>`.