package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/Lofter1/mkm-notifier/mkm"
	"gitlab.com/Lofter1/mkm-notifier/ntfy"
)

// config
var (
	seller       string
	cardsFile    string
	languageCode string
	ntfyURL      string
	repeat       bool
	frequency    int
)

type card struct {
	name      string
	available bool
	url       string
}

func init() {
	flag.StringVar(&seller, "seller", "", "Name of the seller on cardmarket")
	flag.StringVar(
		&cardsFile,
		"file-cards",
		"cards.txt",
		"Path to the file containing the cards you want to search for")
	flag.StringVar(&languageCode,
		"cm-language-code",
		"en",
		"Cardmarket language code (can change results for searches)")
	flag.StringVar(&ntfyURL, "ntfy-url", "", "Topic/Endpoint for ntfy notifications")
	flag.BoolVar(&repeat, "repeat", false, "Repeat request")
	flag.IntVar(&frequency, "frequency", 1, "The frequency to check mkm (in hours)")

	flag.Parse()

	if seller == "" {
		fmt.Println("Seller is required")
		flag.Usage()
		os.Exit(1)
	}
}

func main() {
	cards, err := readCardsFile(cardsFile)

	for {
		if err != nil {
			log.Fatal(err)
		}
		err = searchCards(cards)
		if err != nil {
			log.Fatal(err)
		}

		if !repeat {
			break
		}
		time.Sleep(time.Duration(frequency) * time.Hour)
	}
}

func searchCards(cards []card) error {
	var searchRequest = mkm.SearchRequest{User: seller, LanguageCode: languageCode}

	for i, c := range cards {
		searchRequest.SearchTerm = c.name
		foundCards, err := searchRequest.ExecuteSearch()
		if err != nil {
			return err
		}

		if len(foundCards) > 0 && c.available == false {
			cards[i].available = true
			cards[i].url = foundCards[0].SearchURL
			err := processFoundCard(c)
			if err != nil {
				return err
			}
		} else if len(foundCards) == 0 && c.available == true {
			cards[i].available = false
			cards[i].url = ""
			err := processCardOutOfStock(c)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func processCardOutOfStock(c card) error {
	log.Printf("[%s] [%s] not available anymore", seller, c.name)

	if ntfyURL != "" {
		msg := fmt.Sprintf("[%s] [%s] not available anymore", seller, c.name)
		err := ntfy.SendNtfyNotification(msg, ntfyURL, c.url)
		if err != nil {
			return err
		}
	}
	return nil
}

func processFoundCard(c card) error {
	log.Printf("[%s] [%s] available", seller, c.name)

	if ntfyURL != "" {
		msg := fmt.Sprintf("[%s] [%s] available", seller, c.name)
		err := ntfy.SendNtfyNotification(msg, ntfyURL, c.url)
		if err != nil {
			return err
		}
	}
	return nil
}

func readCardsFile(path string) (foundCards []card, err error) {
	cf, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	cfScanner := bufio.NewScanner(cf)
	for cfScanner.Scan() {
		c := card{name: cfScanner.Text(), available: false}
		foundCards = append(foundCards, c)
	}

	err = cf.Close()
	return foundCards, err
}
