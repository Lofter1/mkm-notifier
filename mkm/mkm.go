package mkm

import (
	"bytes"
	"net/url"
	"text/template"

	"github.com/gocolly/colly"
)

type Card struct {
	Name            string
	Set             string
	Rarity          string
	Condition       string
	Language        string
	Comment         string
	Price           string
	AmountAvailable string
	SearchURL       string
}

var urlTemplate = template.Must(template.New("url").Parse(`https://www.cardmarket.com/{{.LanguageCode}}/Magic/Users/{{.User}}/Offers/Singles?name={{.SearchTerm}}`))

type SearchRequest struct {
	User         string
	SearchTerm   string
	LanguageCode string
}

func (r SearchRequest) ExecuteSearch() ([]Card, error) {
	r.SearchTerm = url.PathEscape(r.SearchTerm)

	url, err := r.getSearchUrlString()
	if err != nil {
		return nil, err
	}

	cards, err := CrawlForCards(*url)
	if err != nil {
		return nil, err
	}

	return cards, nil
}

func CrawlForCards(url string) ([]Card, error) {
	c := colly.NewCollector()
	foundCards := make([]Card, 0)

	c.OnHTML("div.table-body", func(e *colly.HTMLElement) {
		e.ForEach("div.article-row", func(_ int, articleElement *colly.HTMLElement) {
			name := articleElement.ChildText("div.col-seller")
			comment := articleElement.ChildText("div.product-comments")
			price := articleElement.ChildText("div.price-container")
			amount := articleElement.ChildText("div.amount-container")
			condition := articleElement.ChildText("div.product-attributes>a.article-condition")
			attr := articleElement.ChildAttrs("div.product-attributes>span", "data-original-title")
			rarity := attr[0]
			language := attr[1]
			set := articleElement.ChildAttr("div.product-attributes>a.expansion-symbol", "title")

			newCard := Card{
				Name:            name,
				Set:             set,
				Rarity:          rarity,
				Condition:       condition,
				Language:        language,
				Comment:         comment,
				Price:           price,
				AmountAvailable: amount,
				SearchURL:       url,
			}

			foundCards = append(foundCards, newCard)
		})
	})

	err := c.Visit(url)
	if err != nil {
		return nil, err
	}
	return foundCards, nil
}

func (r SearchRequest) getSearchUrlString() (*string, error) {
	var bUrl bytes.Buffer

	err := urlTemplate.Execute(&bUrl, r)

	if err != nil {
		return nil, err
	}

	url := bUrl.String()
	return &url, nil
}
